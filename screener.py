import json
import urllib.request
import urllib.parse
import sys

if len(sys.argv) != 2:
    print('script usage format: python3 screener.py COUNTRY_NAME')
    sys.exit(2)

country = sys.argv[1]

config_file = 'screener.' + country + '.json'
screener_url = 'https://scanner.tradingview.com/' + country + '/scan'

with open(config_file, 'r') as myfile:
    post_data=myfile.read()

data_json = str(post_data)
data_json = data_json.encode('utf-8')

req = urllib.request.Request(screener_url, data=data_json)
contents = urllib.request.urlopen(req).read()

contents_json = json.loads(contents)
#print(json.dumps(contents_json, indent = 1)
print('Total companies:',len(contents_json['data']))

f = open('screener.out', 'w')
f.write(json.dumps(contents_json, indent = 1))
f.close()

print('Data saved to screener.out')
