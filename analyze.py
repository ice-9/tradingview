import json
import pandas as pd
import sys

if len(sys.argv) != 2:
    print('script usage format: python3 analyze.py COUNTRY_NAME')
    sys.exit(2)

country = sys.argv[1]

config_file = 'screener.' + country + '.json'

# read POST data for request to server
with open(config_file, 'r') as screener_file_json:
    screener_data_json=screener_file_json.read()

# read server response
with open('screener.out', 'r') as screener_file_out:
    screener_data_out=screener_file_out.read()

screener_json = json.loads(screener_data_json)
screener_out = json.loads(screener_data_out)

# get useful data from server response
data = screener_out['data']

# prepare list for pandas DataFrame
data_list = []
for row in data:
    data_list.append(row['d'])

pd.set_option('display.max_rows', None)
df = pd.DataFrame(data_list, columns = screener_json['columns'])

# functioni return filtered companies by multipliers values
def df_filter(df, pe_min, pe_max, ps_max, pb_max, close_max = 1000000):
    df_filtered = df[
            (df.loc[:,'price_earnings_ttm'] > pe_min) & 
            (df.loc[:,'price_earnings_ttm'] < pe_max) & 
            (df.loc[:,'price_sales_ratio'] < ps_max) & 
            (df.loc[:,'price_book_ratio'] < pb_max) &
            (df.loc[:,'close'] < close_max)
            ]
    return df_filtered  

# function print companies grouped by economy sectors and filtered by mean values of multipliers
def df_group_filter(df, column):

    df.dropna(subset = ['price_earnings_ttm','price_sales_ratio','price_book_ratio'], inplace = True)
    df_grouped = df.groupby(column)

#    print('|{: >25}|'.format('Sector'), '|{: <5}|'.format('count'), '|{: <20}|'.format('PE median'))
    # for every economy sector
    for sector in df_grouped.groups.keys():
        # get mean values for multipliers
        price_earnings_ttm_median = df_grouped.get_group(sector)['price_earnings_ttm'].median()
        price_sales_ratio_median = df_grouped.get_group(sector)['price_sales_ratio'].median()
        price_book_ratio_median = df_grouped.get_group(sector)['price_book_ratio'].median()
        
        sector_company_count = df_grouped.get_group(sector)['price_earnings_ttm'].count()
        if sector_company_count > 1:
            print('=====================================================================================================================')
            print('=====================================================================================================================')
            print('sector:',sector,', count:',sector_company_count,', PE median: {: .1f}'.format(price_earnings_ttm_median))
            # filter grouped companies by median values of multiplers
            df_grouped_filtered_by_median = df_filter(
                    df_grouped.get_group(sector), 
                    0, 
                    price_earnings_ttm_median, 
                    price_sales_ratio_median, 
                    price_book_ratio_median,
                    close_max = 5000)
            print(df_grouped_filtered_by_median)

#    print(df_grouped.get_group('Health Services'))

df_group_filter(df, 'sector')

print('=====================================================================================================================')
print('=====================================================================================================================')
print('Компании отфильтрованые по критериям 0<PE<5, PS<1, PB<1:')
print(df_filter(df, 0, 5, 1, 1))
